package com.emids.healthinsurence;

import com.emids.healthinsurence.model.User;

public class CalculateInsurancePremium {
	private static double agePremium;
	private static double genderPremium;
	private static double habitPremium;
	private static double currentHealthPremium;
	/*
	 * data: Norman Male 34 No No No Yes No Yes Yes No
	 THE PROBLEM

Norman Gomes is looking for a health insurance quote using this application.


INPUT


Name: Norman Gomes
Gender: Male
Age: 34 years
Current health:


Hypertension: No
Blood pressure: No
Blood sugar: No
Overweight: Yes


Habits:


Smoking: No
Alcohol: Yes
Daily exercise: Yes
Drugs: No






OUTPUT


Health Insurance Premium for Mr. Gomes: Rs. 6,856



BUSINESS RULES


Base premium for anyone below the age of 18 years = Rs. 5,000
% increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10% | 40+ -> 20% increase every 5 years
Gender rule: Male vs female vs Other % -> Increase 2% over standard slab for Males
Pre-existing conditions (Hypertension | Blook pressure | Blood sugar | Overweight) -> Increase of 1% per condition
Habits


Good habits (Daily exercise) -> Reduce 3% for every good habit
Bad habits (Smoking | Consumption of alcohol | Drugs) -> Increase 3% for every bad habit





WHAT DO YOU NEED TO DO IF YOU ARE DOING THIS OFFLINE?


You could use the following tools to solve this problem


IDE: Any IDE 
Java 8
Tomcat
JBoss
Git
My SQL
Postgre SQL
Sublime Text


Create a GitLab ID in case you do not have one
Create your own GitLab repo. Make it open acecss
Keep adding code that you write in git
Unit test the application
Setup and run the application
Commit code to your GitLab repo and share the URL
Add @kuldip-emids to the repo as developer
In case you have questions feel free to reach out
	 */

	public static void main(String[] args) throws Exception {

		if (args != null && args.length == 11) {

			User user = getUserData(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8],
					args[9], args[10]);

			agePremium = calculatePremiumBasedOnAge(user);
			System.out.println("Age Health Premium :" + agePremium);
			genderPremium = calculatePremiumBasedOnGender(user);
			System.out.println("Gender Health Premium :" + genderPremium);

			habitPremium = calculatePremiumBasedOnHabits(user);

			System.out.println("Health Insurance Premium for " + ":" + habitPremium);

			currentHealthPremium = calculatePremiumBasedOnCurrentHealth(user);
			System.out.println(" Health Premium :" + currentHealthPremium);

		} else
			throw new Exception("Please enter valid number of arguments ");

	}

	public static User getUserData(String arg1, String arg2, String arg3, String arg4, String arg5, String arg6,
			String arg7, String arg8, String arg9, String arg10, String arg11) {
		User user = new User();
		user.setBasePremium(5000.00f);

		user.setName(arg1);

		user.setGender(arg2);

		user.setAge(Integer.parseInt(arg3));

		user.getCurrentHealth().setHyperTension(arg4);

		user.getCurrentHealth().setBloodPressure(arg5);

		user.getCurrentHealth().setBloodSugar(arg6);

		user.getCurrentHealth().setOverWeight(arg7);

		user.getHabits().setSmoking(arg8);

		user.getHabits().setAlcohol(arg9);

		user.getHabits().setDailyExercise(arg10);

		user.getHabits().setDrugs(arg11);

		return user;

	}

	public static float calculatePremiumBasedOnAge(User user) {
		if (user.getAge() >= 0 && user.getAge() < 18)
			agePremium = user.getBasePremium();

		if (user.getAge() >= 18 && user.getAge() < 25)
			agePremium = user.getBasePremium() * (0.1);

		if (user.getAge() >= 25 && user.getAge() < 30)
			agePremium = user.getBasePremium() * (0.1);

		if (user.getAge() >= 30 && user.getAge() < 35)
			agePremium = user.getBasePremium() * (0.1);
		if (user.getAge() >= 35 && user.getAge() < 40)
			agePremium = user.getBasePremium() * (0.1);

		if (user.getAge() >= 40 && user.getAge() < 45)
			agePremium = user.getBasePremium() * (0.2);

		if (user.getAge() >= 45 && user.getAge() < 50)
			agePremium = user.getBasePremium() * (0.2);
		if (user.getAge() >= 50 && user.getAge() < 55)
			agePremium = user.getBasePremium() * (0.2);
		if (user.getAge() >= 55 && user.getAge() < 60)
			agePremium = user.getBasePremium() * (0.2);
		if (user.getAge() >= 60 && user.getAge() < 65)
			agePremium = user.getBasePremium() * (0.2);
		if (user.getAge() >= 65 && user.getAge() < 70)
			agePremium = user.getBasePremium() * (0.2);
		if (user.getAge() >= 70 && user.getAge() < 75)
			agePremium = user.getBasePremium() * (0.2);
		if (user.getAge() >= 75 && user.getAge() < 80)
			agePremium = user.getBasePremium() * (0.2);
		if (user.getAge() >= 80 && user.getAge() < 85)
			agePremium = user.getBasePremium() * (0.2);
		if (user.getAge() >= 85 && user.getAge() < 90)
			agePremium = user.getBasePremium() * (0.2);
		if (user.getAge() >= 90 && user.getAge() < 95)
			agePremium = user.getBasePremium() * (0.2);
		if (user.getAge() >= 95 && user.getAge() < 100)
			agePremium = user.getBasePremium() * (0.2);

		return (float) agePremium;

	}

	public static float calculatePremiumBasedOnGender(User user) {
		if (user.getGender().equalsIgnoreCase("Male"))
			genderPremium = user.getBasePremium() * (0.02);

		return (float) genderPremium;
	}

	public static float calculatePremiumBasedOnHabits(User user) {

		if (user.getHabits().getDailyExercise().equalsIgnoreCase("Yes"))
			habitPremium = user.getBasePremium() * (0.03);

		if (user.getHabits().getSmoking().equalsIgnoreCase("Yes"))
			habitPremium = user.getBasePremium() * (0.03);

		if (user.getHabits().getAlcohol().equalsIgnoreCase("Yes"))
			habitPremium = user.getBasePremium() * (0.03);

		if (user.getHabits().getDrugs().equalsIgnoreCase("Yes"))
			habitPremium = user.getBasePremium() * (0.03);
		return (float) habitPremium;
	}

	public static float calculatePremiumBasedOnCurrentHealth(User user) {
		if (user.getCurrentHealth().getHyperTension().equalsIgnoreCase("Yes"))
			currentHealthPremium = user.getBasePremium() * (0.01);

		if (user.getCurrentHealth().getBloodPressure().equalsIgnoreCase("Yes"))
			currentHealthPremium = user.getBasePremium() * (0.01);

		if (user.getCurrentHealth().getBloodSugar().equalsIgnoreCase("Yes"))
			currentHealthPremium = user.getBasePremium() * (0.01);

		if (user.getCurrentHealth().getOverWeight().equalsIgnoreCase("Yes"))
			currentHealthPremium = user.getBasePremium() * (0.01);

		return (float) currentHealthPremium;
	}
}


