package com.emids.healthinsurence.model;

public class User {

	private String name;
	private String gender;
	private int age;
	private float basePremium;

	private CurrentHealth currentHealth = new CurrentHealth();
	private Habits habits = new Habits();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public CurrentHealth getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}
	public Habits getHabits() {
		return habits;
	}
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	public float getBasePremium() {
		return basePremium;
	}
	public void setBasePremium(float basePremium) {
		this.basePremium = basePremium;
	}
	


}
